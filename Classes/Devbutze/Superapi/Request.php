<?php
namespace Devbutze\Superapi;

use Devbutze\Superapi\Authentication\User;
use Devbutze\Superapi\Configuration\ConfigurationManager;
use Devbutze\Superapi\Object\ObjectManager;
use Devbutze\Superapi\Routing\Route;

class Request extends \Symfony\Component\HttpFoundation\Request {

	/**
	 * @var array
	 */
	protected $pathSegments;

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var string
	 */
	protected $objectId;

	/**
	 * @var string
	 */
	protected $propertyId;

	/**
	 * @return array
	 */
	public function getPathSegments() {
		if ($this->pathSegments == NULL) {
			$url = $this->getPathInfo();
			$prefix = ObjectManager::$_self->getObject('configurationManager')->getGeneralConfiguration('basePath');
			if (substr($url, 0, strlen($prefix)) === $prefix) {
				$url = substr($url, strlen($prefix));
			}
			$url = trim($url, ' /');
			$this->pathSegments = explode('/', $url);
		}
		return $this->pathSegments;
	}

	/**
	 * @return Route
	 */
	public function getRoute() {
		return $this->route;
	}

	/**
	 * @return User
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	public function setUser($user) {
		$this->user = $user;
	}

	/**
	 * @return string
	 */
	public function getObjectName() {
		return $this->getPathSegments()[0];
	}

	/**
	 * @return mixed
	 */
	public function getObjectId() {
		return ($this->objectId ?: $this->getPathSegments()[1]);
	}

	/**
	 * @param string $objectId
	 * @return mixed
	 */
	public function setObjectId($objectId) {
		$this->objectId = $objectId;
	}

	/**
	 * @return string
	 */
	public function getPropertyId() {
		return ($this->propertyId ?: $this->getPathSegments()[3]);
	}

	/**
	 * @param string $propertyId
	 */
	public function setPropertyId($propertyId) {
		$this->propertyId = $propertyId;
	}

	/**
	 * @return string
	 */
	public function getPropertyName() {
		return $this->getPathSegments()[2];
	}
}