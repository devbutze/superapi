<?php
namespace Devbutze\Superapi\Processor;

/**
 * Class AbstractObjectProcessor
 *
 * may implement pre/post insert/select/update/delete
 *
 * @package Devbutze\Superapi\Processor
 */
abstract class AbstractObjectProcessor {
}