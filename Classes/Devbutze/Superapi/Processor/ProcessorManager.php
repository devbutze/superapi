<?php
namespace Devbutze\Superapi\Processor;

use Devbutze\Superapi\Object\ObjectManager;

class ProcessorManager {

	/**
	 * @param string $object
	 * @param string $method
	 * @param array $payload
	 * @return array|mixed
	 */
	public function preProcess($object, $method, array $payload) {
		return $this->process($object, 'processPre' . ucfirst($method), $payload);
	}

	/**
	 * @param string $object
	 * @param string $method
	 * @param array $payload
	 * @return array|mixed
	 */
	public function postProcess($object, $method, array $payload) {
		return $this->process($object, 'processPost' . ucfirst($method), $payload);
	}

	/**
	 * @param string $object
	 * @param string $method
	 * @param array $payload
	 * @return array|mixed
	 */
	protected function process($object, $method, array $payload) {
		// convert processor name for objects containing _
		if (strstr($object, '_')) {
			$oldObject = $object;
			$object = '';
			foreach (explode('_', $oldObject) as $objectPart) {
				$object .= ucfirst($objectPart);
			}
		}

		$processorObjectName = ObjectManager::$_self->getObject('configurationManager')->getNamespace()
			. '\Processor\\' . ucfirst($object) . 'ObjectProcessor';
		if (class_exists($processorObjectName)) {
			$processor = new $processorObjectName();
			$processorMethodName = $method;
			if (method_exists($processor, $processorMethodName)) {
				return call_user_func(array($processor, $processorMethodName), $payload);
			}
		}

		return $payload;
	}
}