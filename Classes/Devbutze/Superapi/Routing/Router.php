<?php
namespace Devbutze\Superapi\Routing;

use Devbutze\Superapi\Exception\MethodNotAllowedException;
use Devbutze\Superapi\Exception\NoRouteException;
use Devbutze\Superapi\Object\ObjectManager;

class Router {

	/**
	 * @return Route
	 */
	public function getRoute() {
		return $this->buildRoute();
	}

	/**
	 * @return Route
	 */
	protected function buildRoute() {
		$configuration = $this->resolveRouteConfiguration();

		$route = new Route();
		$route->setConfiguration($configuration);
		return $route;
	}

	/**
	 * @return array
	 * @throws MethodNotAllowedException
	 * @throws NoRouteException
	 */
	protected function resolveRouteConfiguration() {
		$request = ObjectManager::$_self->getObject('request');
		$segments = $request->getPathSegments();

		$configuration = ObjectManager::$_self->getObject('configurationManager')->getRoutingConfiguration();
		foreach ($segments as $segment) {

			if (is_numeric($segment)) {
				$segment = '_int_';
			}

			if (array_key_exists($segment, $configuration)) {
				$configuration = $configuration[$segment];
			} else {
				throw new NoRouteException();
			}
		}

		if (!array_key_exists($request->getMethod(), $configuration)) {
			throw new MethodNotAllowedException();
		}

		return $configuration[ObjectManager::$_self->getObject('request')->getMethod()];
	}
}