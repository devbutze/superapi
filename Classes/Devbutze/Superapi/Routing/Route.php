<?php
namespace Devbutze\Superapi\Routing;


class Route {

	/**
	 * @var array
	 */
	protected $configuration;

	/**
	 * @return array
	 */
	public function getConfiguration() {
		return $this->configuration;
	}

	/**
	 * @param array $configuration
	 */
	public function setConfiguration($configuration) {
		$this->configuration = $configuration;
	}

	/**
	 * @param string $key
	 * @return string|array
	 */
	public function get($key) {
		return isset($this->configuration[$key]) ? $this->configuration[$key] : NULL;
	}

	/**
	 * @return string
	 */
	public function getController() {
		$controller = $this->get('controller');
		return ($controller && class_exists($controller)) ? $controller : '\Devbutze\Superapi\Controller\DefaultController';
	}
}