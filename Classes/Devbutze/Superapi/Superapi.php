<?php
namespace Devbutze\Superapi;

use Devbutze\Superapi\Authentication\Provider\AbstractAuthenticationProvider;
use Devbutze\Superapi\Authentication\User;
use Devbutze\Superapi\Configuration\ConfigurationManager;
use Devbutze\Superapi\Controller\AbstractController;
use Devbutze\Superapi\Exception\AbstractException;
use Devbutze\Superapi\Exception\AuthenticationException;
use Devbutze\Superapi\Exception\MethodNotImplementedException;
use Devbutze\Superapi\Mapper\Mapper;
use Devbutze\Superapi\Object\ObjectManager;
use Devbutze\Superapi\Persistence\PersistenceManager;
use Devbutze\Superapi\Persistence\SqlAdapter;
use Devbutze\Superapi\Persistence\SqlQueryBuilder;
use Devbutze\Superapi\Processor\ProcessorManager;
use Devbutze\Superapi\Routing\Router;
use Devbutze\Superapi\Validator\Validator;

class Superapi {

	/**
	 * @param string $configurationPath
	 */
	public function run($configurationPath) {
		$objectManager = new ObjectManager();
		ObjectManager::$_self = $objectManager;

		ObjectManager::$_self->addObject('configurationManager', new ConfigurationManager($configurationPath));

		/** @var Request $request */
		$request = Request::createFromGlobals();
		ObjectManager::$_self->addObject('request', $request);

		/** @var Response $response */
		$response = new Response();
		ObjectManager::$_self->addObject('response', $response);

		$validator = new Validator();
		ObjectManager::$_self->addObject('validator', $validator);

		$sqlAdapter = new SqlAdapter();
		$sqlAdapter->initialize();
		ObjectManager::$_self->addObject('sqlAdapter', $sqlAdapter);

		$sqlQueryBuilder = new SqlQueryBuilder();
		ObjectManager::$_self->addObject('sqlQueryBuilder', $sqlQueryBuilder);

		$persistenceManager = new PersistenceManager();
		ObjectManager::$_self->addObject('persistenceManager', $persistenceManager);

		$processorManager = new ProcessorManager();
		ObjectManager::$_self->addObject('processorManager', $processorManager);

		try {
			$request->setUser(
				$this->authenticateUser($request, $response)
			);

			$response->headers->set('X-T3FeUser', $request->getUser()->getId());

			$mapper = new Mapper();
			ObjectManager::$_self->addObject('mapper', $mapper);

			$router = new Router();
			ObjectManager::$_self->addObject('router', $router);

			$this->callControllerAction($request, $response);

		} catch (AbstractException $e) {
			$statusText = !empty($e->getStatusText()) ? $e->getStatusText() : NULL;
			$response->setStatusCode($e->getStatusCode(), $statusText);
		}

		$response->send();
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @throws AuthenticationException
	 * @throws MethodNotImplementedException
	 */
	protected function callControllerAction($request, $response) {
			// @todo need proper authorization implementation
		if ($request->getUser() === NULL) {
			throw new AuthenticationException();
		}

		$controller = $this->buildController($request, $response);

		$action = strtolower($request->getMethod());
		if (method_exists($controller, $action . 'Action')) {
			call_user_func_array(array($controller, $action . 'Action'), array());
		} else {
			throw new MethodNotImplementedException();
		}
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @return AbstractController
	 */
	protected function buildController(Request $request, Response $response) {
		$controllerClassname = ObjectManager::$_self->getObject('router')->getRoute()->getController();

		/** @var AbstractController $controller */
		$controller = new $controllerClassname($request, $response);
		return $controller;
	}

	/**
	 * @return User|null
	 */
	protected function authenticateUser() {
		$user = NULL;
		foreach (ObjectManager::$_self->getObject('configurationManager')->getAuthenticationProvider() as $providerClassname) {
			if (class_exists($providerClassname)) {
				/** @var AbstractAuthenticationProvider $provider */
				$provider = new $providerClassname;
				if ($provider->canAuthenticate()) {
					$user = $provider->authenticate();
					if ($user) {
						ObjectManager::$_self->addObject('user', $user);
						return $user;
					}
				}
			}
		}
	}
}