<?php
namespace Devbutze\Superapi\Configuration;

class ConfigurationManager {

	/**
	 * @var string
	 */
	protected $configurationPath = '';

	/**
	 * @var array
	 */
	protected $inline = array();

	/**
	 * @var array
	 */
	protected $mappingConfiguration = array();

	/**
	 * @var array
	 */
	protected $routingConfiguration = array();

	/**
	 * @var array
	 */
	protected $generalConfiguration = array();

	/**
	 * @var \Symfony\Component\Yaml\Parser
	 */
	protected $yamlParser;

	/**
	 * @param string $configurationPath
	 * @throws \Exception
	 */
	public function __construct($configurationPath) {
		if (!file_exists($configurationPath)) {
			throw new \Exception('Cant open configuration path "' . $configurationPath . '"', 1421920461);
		}

		$configurationPath = rtrim($configurationPath, ' /');

		$this->yamlParser = new \Symfony\Component\Yaml\Parser();

		$this->generalConfiguration = array_merge(
				$this->readConfigurationFile($configurationPath . '/config.yaml'),
				$this->readOverlayConfigurationFile($configurationPath . '/config.yaml')
		);
		$this->mappingConfiguration = $this->readConfigurationFile($configurationPath . '/mapping.yaml');
		$this->routingConfiguration = $this->readConfigurationFile($configurationPath . '/routing.yaml');
	}

	/**
	 * @param string $inline
	 */
	public function setInline($inline) {
		$this->inline = explode(' ', $inline);
	}

	/**
	 * @return array
	 */
	public function getInline() {
		return $this->inline;
	}

	/**
	 * @param string $configurationFile
	 * @return array
	 * @throws \Exception
	 */
	protected function readConfigurationFile($configurationFile) {
		if (!file_exists($configurationFile)) {
			throw new \Exception('Cant open configuration "' . $configurationFile . '"', 1421920462);
		}
		return $this->yamlParser->parse(file_get_contents($configurationFile));
	}

	/**
	 * @param $configurationFile
	 * @return array
	 */
	protected function readOverlayConfigurationFile($configurationFile) {
		$path = explode('/', trim(dirname($configurationFile), '/'));
		$pathSegment = $path[count($path) - 6];
		$uname = php_uname('n');

		list($suffix) = array_reverse(explode('.', $configurationFile));
		$overwriteFile = substr($configurationFile, 0, ((strlen($suffix) + 1) * -1)) .
				'_' . $uname .
				'_' . $pathSegment .
				'.' . $suffix;
		if (file_exists($overwriteFile)) {
			return $this->readConfigurationFile($overwriteFile);
		}
		return array();
	}

	/**
	 * @return array
	 */
	public function getGeneralConfiguration($path = NULL) {
		if ($path === NULL) {
			return $this->generalConfiguration;
		}

		$configuration = $this->generalConfiguration;
		foreach (explode('.', $path) as $segment) {
			$configuration = $configuration[$segment];
		}
		return $configuration;
	}

	/**
	 * @param string $object
	 * @return array
	 */
	public function getMappingConfiguration($object = '') {
		return ($object ? $this->mappingConfiguration[$object] : $this->mappingConfiguration);
	}

	/**
	 * @return array
	 */
	public function getRoutingConfiguration() {
		return $this->routingConfiguration;
	}

	/**
	 * @return array
	 */
	public function getAuthenticationProvider() {
		return $this->getGeneralConfiguration('authenticationProvider');
	}

	/**
	 * @return string
	 */
	public function getNamespace() {
		return $this->getGeneralConfiguration('namespace');
	}
}