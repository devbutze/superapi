<?php
namespace Devbutze\Superapi\Mapper;

use Devbutze\Superapi\Exception\InternalServerErrorException;
use Devbutze\Superapi\Object\ObjectManager;

class Mapper {

	/**
	 * @var array
	 */
	protected $tableMaps = array();

	/**
	 * @var array
	 */
	protected $fieldMaps = array();

	/**
	 * @var array
	 */
	protected $propertyAliases = array();

	public function __construct() {
		$this->processMappingConfiguration();
	}

	/**
	 * @param $object
	 * @return mixed
	 */
	public function getTableMap($object) {
		return ($this->tableMaps[$object] ?: $object);
	}

	/**
	 * @param string $object
	 * @param string $property
	 * @return array
	 * @throws InternalServerErrorException
	 */
	public function getFieldMap($object, $property) {
		if (!$this->fieldMaps[$object.'.'.$property]) {
			throw new InternalServerErrorException('Fieldmap ' . $object . '.' . $property . ' not found.');
		}

		return $this->fieldMaps[$object.'.'.$property];
	}

	public function getTablenameForObject($object) {
		$tableMap = $this->getTableMap($object);
		if (isset($tableMap['nameInSource'])) {
			$tablename = $tableMap['nameInSource'];
		} else {
			$tablename = $object;
		}

		return $tablename;
	}

	public function getFieldnameForProperty($object, $property) {
		if (!empty($this->fieldMaps[$object.'.'.$property]['nameInSource'])) {
			return $this->fieldMaps[$object.'.'.$property]['nameInSource'];
		} else {
			return $property;
		}
	}

	public function getPropertyNameForField($table, $field) {
		return ($this->propertyAliases[$table.'.'.$field] ?: $field);
	}

	/**
	 * @param string $object
	 * @param string $property
	 * @return mixed
	 * @throws InternalServerErrorException
	 */
	public function getObjectNameFromPropertyName($object, $property) {
		$fieldmap = $this->getFieldMap($object, $property);

		return ($fieldmap['relation']['to'] ?: $property);
	}

	/**
	 * @param string $object
	 * @param array $row
	 * @param bool $isSubobject
	 * @return mixed
	 */
	public function mapRowToObject($object, $row, $isSubobject = FALSE) {
		foreach ($row as $field => $value) {
			$propertyName = $this->getPropertyNameForField($this->getTablenameForObject($object), $field);

			// remove hidden fields
			if (isset($this->tableMaps[$object]['hiddenFields'])) {
				if (in_array($field, $this->tableMaps[$object]['hiddenFields'])) {
					unset($row[$field]);
					continue;
				}
			}

			// let's see, if there is - at least - some mapping configuration
			if (!empty($this->fieldMaps[$object.'.'.$propertyName])) {
				$fieldConfig = $this->fieldMaps[$object.'.'.$propertyName];

				if (!$isSubobject && !empty($fieldConfig['relation'])) {
					$row[$field] = $this->mapRelation(
						$row,
						$field,
						$fieldConfig['relation']
					);
				} elseif (!empty($fieldConfig['map'])) {
					$row[$field] = ($fieldConfig['map'][$value] ?: ($fieldConfig['map']['default'] ?: $value));
				}
			}

			if ($row[$field] === '') {
				$row[$field] = NULL;
			}

			if ($propertyName !== $field) {
				$row[$propertyName] = $row[$field];
				unset($row[$field]);
			}
		}

		return $row;
	}

	/**
	 * @param array $parentObject
	 * @param array $relationConfiguration
	 * @return mixed
	 */
	public function mapRelation($parentObject, $field, $relationConfiguration) {
		$persistenceManager = ObjectManager::$_self->getObject('persistenceManager');
		$relation = array();

		/**
		 * @todo implement lookups
		 * * default: take the uid from the db field and use as 'id' in related table
		 * * csv: (do not use that!) split the db field at ',' and use each number as 'id' in related table
		 */
		switch ($relationConfiguration['lookup']) {
			case 'csv':
				if ($parentObject[$field]) {
					$relation = $persistenceManager->getSubObjects(
						$relationConfiguration['to'],
						array('id' => explode(',', $parentObject[$field])),
						!isset($relationConfiguration['loadSubobjects'])
				);
				}
				break;
			case 'mm':
				$relationTableMap = $this->getTableMap($relationConfiguration['via']);

				$subObjects = $persistenceManager->getSubObjects(
					$this->getTablenameForObject($relationConfiguration['via']),
					array($relationTableMap['localKey'] => $parentObject['id']),
					!isset($relationConfiguration['loadSubobjects'])
				);

				if (!empty($subObjects)) {
					foreach ($subObjects as $subObject) {
						$subObjectIds[] = $subObject[$relationTableMap['foreignKey']];
					}
				}

				if (!empty($subObjectIds)) {
					$relation = $persistenceManager->getSubObjects(
						$relationConfiguration['to'],
						array('id' => $subObjectIds),
						!isset($relationConfiguration['loadSubobjects'])
					);
				}
				break;
			case 'reverse':
				$relation = $persistenceManager->getSubObjects(
					$relationConfiguration['to'],
					array($relationConfiguration['foreignKey'] => $parentObject['id']),
					!isset($relationConfiguration['loadSubobjects'])
				);
				break;
			default:
				if ($parentObject[$field]) {
					$relation = $persistenceManager->getSubObject(
						$relationConfiguration['to'],
						array('id' => $parentObject[$field]),
						!isset($relationConfiguration['loadSubobjects'])
					);
				}
		}

		return $relation;
	}

	/**
	 * @return void
	 */
	protected function processMappingConfiguration() {
		$mappingConfiguration = ObjectManager::$_self->getObject('configurationManager')->getMappingConfiguration();

		foreach ($mappingConfiguration as $object => $objectConfig) {
			$this->tableMaps[$object] = $objectConfig;
			$this->tableMaps[$object]['nameInSource'] = ($objectConfig['nameInSource'] ?: $object);
			$this->tableMaps[$object]['hiddenFields'] = ($objectConfig['hiddenFields'] ?: array());

			if ($objectConfig['fields']) {
				foreach ($objectConfig['fields'] as $field => $fieldConfig) {
					$this->fieldMaps[$object.'.'.$field] = $fieldConfig;
					if (!empty($fieldConfig['nameInSource'])) {
						$this->propertyAliases[$this->getTablenameForObject($object).'.'.$fieldConfig['nameInSource']] = $field;
					}
				}
			}
		}
	}
}