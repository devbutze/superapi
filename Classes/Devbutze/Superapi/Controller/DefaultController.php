<?php
namespace Devbutze\Superapi\Controller;

use Devbutze\Superapi\Exception\AbstractException;
use Devbutze\Superapi\Exception\InternalServerErrorException;
use Devbutze\Superapi\Exception\MethodNotAllowedException;
use Devbutze\Superapi\Mapper\Mapper;
use Devbutze\Superapi\Object\ObjectManager;
use Devbutze\Superapi\Persistence\PersistenceManager;
use Devbutze\Superapi\Request;
use Symfony\Component\HttpFoundation\Response;
use Devbutze\Superapi\Validator\Validator;

class DefaultController extends AbstractController {

	/**
	 * @var Request
	 */
	protected $request;

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * @var Validator
	 */
	protected $validator;

	/**
	 * @param Request $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response) {
		$this->request = $request;
		$this->response = $response;
		$this->validator = ObjectManager::$_self->getObject('validator');
	}

	/**
	 *
	 */
	public function getAction() {
		/** @var Mapper $mapper */
		$mapper = ObjectManager::$_self->getObject('mapper');
		/** @var PersistenceManager $persistenceManager */
		$persistenceManager = ObjectManager::$_self->getObject('persistenceManager');

		if ($this->request->getPropertyName()) {
			if ($this->request->getPropertyId()) {
				$propertyName = $mapper->getObjectNameFromPropertyName($this->request->getObjectName(), $this->request->getPropertyName());
				$objects = $persistenceManager->getobjects(
					$propertyName,
					array('uid' => $this->request->getPropertyId())
				);
			} else {
				/**
				 * no propertyId given, so resolve relation
				 */
				$propertyFieldMap = $mapper->getFieldMap(
					$this->request->getObjectName(),
					$this->request->getPropertyName()
				);

				if (!empty($propertyFieldMap['relation'])) {
					// use getSubObject() here to avoid loading child objects
					$parentObject = $persistenceManager->getSubObject(
						$this->request->getObjectName(),
						array('id' => $this->request->getObjectId()));

					$objects = $mapper->mapRelation(
						$parentObject,
						$this->request->getPropertyName(),
						$propertyFieldMap['relation']
					);
				} else {
					throw new MethodNotAllowedException('Property "' . $this->request->getPropertyName() . '" is not a relation and thus not accessible.');
				}
			}

			$objects = ObjectManager::$_self->getObject('processorManager')
				->preProcess($this->request->getPropertyName(), 'return', $objects);
		} else {
			$objects = $persistenceManager->getobjects(
				$this->request->getObjectName(),
				array('uid' => $this->request->getObjectId())
			);

			$objects = ObjectManager::$_self->getObject('processorManager')->preProcess($this->request->getObjectName(), 'return', $objects);
		}

		$this->response->setData(array('objects' => $objects));
	}

	public function putAction() {
		/** @var PersistenceManager $persistenceManager */
		$persistenceManager = ObjectManager::$_self->getObject('persistenceManager');

		$data = $this->getBodyData();

		if ($this->request->getPropertyName()) {
			$result = $persistenceManager->updateSubObject(
				$this->request->getObjectName(),
				$this->request->getObjectId(),
				$this->request->getPropertyName(),
				$data,
				$this->request->getPropertyId()
			);

			if ($result) {
				$this->request->setPropertyId($result);
			}
		} else {
			// @todo use updateObject instead
			$result = $persistenceManager->updateRow(
				$this->request->getObjectName(),
				array('uid' => $this->request->getObjectId()),
				$data
			);
		}

		if ($result) {
			return $this->getAction();
		} else {
			$this->response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, 'Database Update failed.');
		}
	}

	/**
	 * @throws AbstractException
	 */
	public function postAction() {
		if ($this->request->getObjectId() &&
			(!$this->request->getPropertyName() || $this->request->getPropertyId())) {
			/**
			 * hack to deal with backbone firing post requests for put
			 */
			return $this->putAction();
		}

		/**
		 * deal with file uploads
		 */
		if ($this->request->files->count() > 0) {
			return $this->fileUploadAction();
		}

		/** @var PersistenceManager $persistenceManager */
		$persistenceManager = ObjectManager::$_self->getObject('persistenceManager');
		$data = $this->getBodyData();

		if ($this->request->getPropertyName()) {
			$this->validator->validate(
				$this->request->getPropertyName(),
				'add',
				$data
			);

			$result = $persistenceManager->addSubObject(
				$this->request->getObjectName(),
				$this->request->getObjectId(),
				$this->request->getPropertyName(),
				$data
			);

			if ($result) {
				$this->request->setPropertyId($result);
			}
		} else {
			$this->validator->validate(
				$this->request->getObjectName(),
				'add',
				$data
			);
			$result = $persistenceManager->addObject(
				$this->request->getObjectName(),
				$data
			);

			if ($result) {
				$this->request->setObjectId($result);
			}
		}

		if ($result) {
			return $this->getAction();
		} else {
			$this->response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, 'Database Update failed.');
		}
	}

	public function deleteAction() {
		/** @var PersistenceManager $persistenceManager */
		$persistenceManager = ObjectManager::$_self->getObject('persistenceManager');

		if ($this->request->getPropertyName()) {
			$result = $persistenceManager->removeSubObject(
				$this->request->getObjectName(),
				$this->request->getObjectId(),
				$this->request->getPropertyName(),
				$this->request->getPropertyId()
			);
		} else {
			$result = $persistenceManager->removeObject(
				$this->request->getObjectName(),
				$this->request->getObjectId()
			);
		}

		if ($result) {
			$this->response->setData(array('status' => 'ok'));
		} else {
			$this->response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, 'Deletion failed.');
		}
	}

	public function fileUploadAction() {
		if ($this->request->files->count() !== 1) {
			throw new InternalServerErrorException('Only upload one file at a time.');
		}

		/** @var PersistenceManager $persistenceManager */
		$persistenceManager = ObjectManager::$_self->getObject('persistenceManager');
		$configurationManager = ObjectManager::$_self->getObject('configurationManager');

		if ($this->request->getPropertyName()) {
			$parentMappingConfiguration = $configurationManager->getMappingConfiguration($this->request->getObjectName());
			$fieldConfiguration = $parentMappingConfiguration['fields'][$this->request->getPropertyName()];

			if (!$fieldConfiguration['relation']['to']) {
				throw new InternalServerErrorException($this->request->getObjectName() . '.' . $this->request->getPropertyName() . ' is not a relation.');
			}

			$fileMappingConfiguration = $configurationManager->getMappingConfiguration($fieldConfiguration['relation']['to']);

			if (!$fileMappingConfiguration['additionalConfiguration']['uploadPath']) {
				throw new InternalServerErrorException('No upload folder defined.');
			}

			$file = array_pop($this->request->files->all());

			// make sure every filename is unique
			$newFilename = str_replace('.' . $file->getClientOriginalExtension(), '', $file->getClientOriginalName())
				. '_' . substr(md5(time() + rand()), 0, 5)
				. '.' . $file->getClientOriginalExtension();

			$file->move(
				rtrim($configurationManager->getGeneralConfiguration('rootPath'), ' /') . '/'
					. $fileMappingConfiguration['additionalConfiguration']['uploadPath'],
				$newFilename
			);

			$result = $persistenceManager->addSubObject(
				$this->request->getObjectName(),
				$this->request->getObjectId(),
				$this->request->getPropertyName(),
				array(
					'original_name' => $file->getClientOriginalName(),
					'name' => $newFilename
			));

			if ($result) {
				$this->request->setPropertyId($result);
			}
		} else {
			throw new InternalServerErrorException('Uploading files without parent object is not supported.');
		}

		if ($result) {
			return $this->getAction();
		} else {
			$this->response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, 'File upload failed.');
		}
	}

	/**
	 * @return mixed
	 * @throws AbstractException
	 */
	protected function getBodyData() {
		/** @var Request $this->request */
		$this->request = ObjectManager::$_self->getObject('request');

		$return = json_decode($this->request->getContent(), TRUE);

		if ($this->request->getContent()
			&& $this->request->getContent() !== '{}'
			&& !$return
		) {
			throw new AbstractException('Sent JSON could not be decoded.');
		}

		return $return;
	}
}