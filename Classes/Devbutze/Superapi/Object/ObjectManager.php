<?php
namespace Devbutze\Superapi\Object;

class ObjectManager {

	/**
	 * @var ObjectManager
	 */
	static public $_self;

	/**
	 * @var array
	 */
	protected $objects = array();

	/**
	 * @param $identifier
	 * @param $object
	 */
	public function addObject($identifier, $object) {
		$this->objects[$identifier] = $object;
	}

	/**
	 * @param $identifier
	 * @return null
	 */
	public function getObject($identifier) {
		return $this->objects[$identifier] ?: NULL;
	}
}