<?php
namespace Devbutze\Superapi\Authentication\Provider;

use Devbutze\Superapi\Authentication\User;
use Devbutze\Superapi\Object\ObjectManager;

class Typo3FeSessionAuthenticationProvider extends AbstractAuthenticationProvider {

	/**
	 * @return bool
	 */
	public function canAuthenticate() {
		return ObjectManager::$_self->getObject('request')->cookies->has('fe_typo_user');
	}

	/**
	 * @return User
	 */
	public function authenticate() {
		$sessionId = ObjectManager::$_self->getObject('request')->cookies->getAlnum('fe_typo_user');
		if (!empty($sessionId)) {
			$userData = $this->getUserRepository()->findByFeSessionId($sessionId);
			if (!empty($userData)) {
				return User::createFromArray($userData);
			}
		}
	}
}