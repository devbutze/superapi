<?php
namespace Devbutze\Superapi\Authentication\Provider;
use Devbutze\Superapi\Authentication\User;
use Devbutze\Superapi\Persistence\UserRepository;

abstract class AbstractAuthenticationProvider {

	/**
	 * @return UserRepository
	 */
	public function getUserRepository() {
		$userRepository = new UserRepository();
		return $userRepository;
	}

	/**
	 * @return bool
	 */
	abstract public function canAuthenticate();

	/**
	 * @return User
	 */
	abstract public function authenticate();

}
