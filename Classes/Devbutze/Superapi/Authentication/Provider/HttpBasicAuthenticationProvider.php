<?php
namespace Devbutze\Superapi\Authentication\Provider;

use Devbutze\Superapi\Authentication\User;

class HttpBasicAuthenticationProvider extends AbstractAuthenticationProvider {

	/**
	 * @return bool
	 */
	public function canAuthenticate() {
		return FALSE; // TODO
		return $this->getRequest()->server->getAlnum('PHP_AUTH_USER') != ''
				&& $this->getRequest()->server->getAlnum('PHP_AUTH_PW') != '';
	}

	/**
	 * @return User
	 */
	public function authenticate() {
	}
}