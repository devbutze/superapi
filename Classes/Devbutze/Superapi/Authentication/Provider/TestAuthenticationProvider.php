<?php
namespace Devbutze\Superapi\Authentication\Provider;

use Devbutze\Superapi\Authentication\User;

class TestAuthenticationProvider extends AbstractAuthenticationProvider {

	/**
	 * @return bool
	 */
	public function canAuthenticate() {
		return TRUE;
	}

	/**
	 * @return User
	 */
	public function authenticate() {
		return User::createFromArray(array(
			'uid' => 1,
			'username' => 'Testuser'
		));
	}
}