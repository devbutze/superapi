<?php
namespace Devbutze\Superapi\Authentication;

class User {


	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var int
	 */
	protected $id;

	// TODO move to Stagedeal\Api Package
	static $extbaseTypeToRoleMapping = array(
		'Tx_Stagedeal_Artist' => 'Artist',
		'Tx_Stagedeal_Agent' => 'Agent',
		'Tx_Stagedeal_Producer' => 'Producer',
		'Tx_Stagedeal_PrivateProducer' => 'PrivateProducer',
		'Tx_Stagedeal_HeadOffice' => 'HeadOffice',
		'Tx_Stagedeal_Evaluator' => 'Evaluator',
	);

	/**
	 * @var array
	 */
	protected $roles = array();

	/**
	 * @var int
	 */
	protected $language = 'en';

	/**
	 * @param $userData
	 * @return static
	 * @throws \Exception
	 */
	public static function createFromArray($userData) {
		if (empty($userData['uid'])) {
			throw new \Exception('invalid user data');
		}
		$user = new static();

		$user->setId($userData['uid']);

		if(!empty($userData['username'])) {
			$user->setName($userData['username']);
		}

		if ($userData['language']) {
			$user->setLanguage($userData['language']);
		}


		if (!empty($userData['tx_extbase_type'])) {
			$user->addRole(self::$extbaseTypeToRoleMapping[$userData['tx_extbase_type']]);
		}

		// TODO role based on FeUserGroups

		return $user;
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getLanguage() {
		return $this->language;
	}

	/**
	 * @param int
	 */
	public function setLanguage($language) {
		$this->language = $language;
	}

	/**
	 * @param string $role
	 */
	public function addRole($role) {
		$this->roles[] = $role;
	}

	/**
	 * @param string $role
	 * @return bool
	 */
	public function hasRole($role) {
		return in_array($role, $this->roles);
	}

	/**
	 * @return array
	 */
	public function getRoles() {
		return $this->roles;
	}

	public function isRegistered() {
		return !(substr($this->getId(), 0, 3) === 'ses');
	}

	public function __toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'isRegistered' => $this->isRegistered(),
			'language' => $this->getLanguage(),
			'roles' => $this->getRoles(),
		);
	}
}