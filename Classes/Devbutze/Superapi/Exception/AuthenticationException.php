<?php
namespace Devbutze\Superapi\Exception;

use Devbutze\Superapi\Response;

class AuthenticationException extends AbstractException {
	/**
	 * @var int
	 */
	protected $statusCode = Response::HTTP_FORBIDDEN;

	/**
	 * @var string
	 */
	protected $statusText = '';
}