<?php
namespace Devbutze\Superapi\Exception;

use Devbutze\Superapi\Response;

class InternalServerErrorException extends AbstractException {
	/**
	 * @var int
	 */
	protected $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;

	/**
	 * @var string
	 */
	protected $statusText = '';
}