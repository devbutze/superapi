<?php
namespace Devbutze\Superapi\Exception;

use Devbutze\Superapi\Response;

class NoRouteException extends AbstractException {
	/**
	 * @var int
	 */
	protected $statusCode = Response::HTTP_NOT_FOUND;

	/**
	 * @var string
	 */
	protected $statusText = 'Route not found';
}