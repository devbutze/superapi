<?php
namespace Devbutze\Superapi\Exception;

use Devbutze\Superapi\Response;

class MethodNotAllowedException extends AbstractException {
	/**
	 * @var int
	 */
	protected $statusCode = Response::HTTP_METHOD_NOT_ALLOWED;

	/**
	 * @var string
	 */
	protected $statusText = '';
}