<?php
namespace Devbutze\Superapi\Exception;

use Devbutze\Superapi\Response;

class RelationNotFoundException extends AbstractException {
	/**
	 * @var int
	 */
	protected $statusCode = Response::HTTP_NOT_FOUND;

	/**
	 * @var string
	 */
	protected $statusText = 'The given relation was not found.';
}