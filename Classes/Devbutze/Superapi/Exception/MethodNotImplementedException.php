<?php
namespace Devbutze\Superapi\Exception;

use Devbutze\Superapi\Response;

class MethodNotImplementedException extends AbstractException {
	/**
	 * @var int
	 */
	protected $statusCode = Response::HTTP_METHOD_NOT_ALLOWED;

	/**
	 * @var string
	 */
	protected $statusText = 'Method Not Implemented';
}