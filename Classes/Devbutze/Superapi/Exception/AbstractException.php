<?php
namespace Devbutze\Superapi\Exception;

use Devbutze\Superapi\Response;

class AbstractException extends \Exception {

	/**
	 * @var int
	 */
	protected $statusCode = Response::HTTP_BAD_REQUEST;

	/**
	 * @var string
	 */
	protected $statusText = '';

	/**
	 * @return int
	 */
	public function getStatusCode() {
		return $this->statusCode;
	}

	/**
	 * @param int $statusCode
	 */
	public function setStatusCode($statusCode) {
		$this->statusCode = $statusCode;
	}

	/**
	 * @return string
	 */
	public function getStatusText() {
		return $this->statusText;
	}

	/**
	 * @param string $statusText
	 */
	public function setStatusText($statusText) {
		$this->statusText = $statusText;
	}

	/**
	 * @param string $statusText
	 */
	public function __construct($statusText = '', $code = 0) {
		if (!empty($statusText)) {
			$this->setStatusText($statusText);
		}
		parent::__construct($statusText, $code);
	}

}