<?php
namespace Devbutze\Superapi\Persistence;

use Devbutze\Superapi\Object\ObjectManager;

class SqlAdapter {

	/**
	 * @var bool
	 */
	protected $_isInitialized = FALSE;

	/**
	 * @var \mysqli
	 */
	protected $database;

	public function initialize() {
		if (!$this->_isInitialized) {
			$databaseConfig = ObjectManager::$_self->getObject('configurationManager')
				->getGeneralConfiguration('database');
			$this->database = new \mysqli(
				$databaseConfig['host'],
				$databaseConfig['username'],
				$databaseConfig['password'],
				$databaseConfig['database']
			);
			$this->_isInitialized = TRUE;
		}
	}

	public function countByQuery($query) {
		$result = $this->query($query);

		return $result->num_rows;
	}

	public function findByQuery($query) {
		$result = $this->query($query);

		$return = array();
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$return[] = $row;
			}
		}

		return $return;
	}

	public function query($query) {
		$this->initialize();

		$result = $this->database->query($query);
		if (!$result) {
			// @todo wtf?! handle errors, please ...
			var_dump($query, $this->database->error_list);
		}

		return $result;
	}

	public function getLastInsertId() {
		return $this->database->insert_id;
	}

	/**
	 * @param string $string
	 * @return string
	 */
	public function escape($string) {
		return $this->database->real_escape_string($string);
	}
}