<?php
namespace Devbutze\Superapi\Persistence;

use Devbutze\Superapi\Object\ObjectManager;

class UserRepository {

	/**
	 * @param $sessionId
	 * @return array
	 */
	public function findByFeSessionId($sessionId) {
		$query =
			'SELECT fe_users.* ' .
			'FROM fe_users, fe_sessions '.
			'WHERE fe_users.uid = fe_sessions.ses_userid ' .
			'AND fe_users.deleted = 0 ' .
			'AND fe_users.disable = 0 ' .
			'AND ses_name = "fe_typo_user" ' .
			'AND ses_id = "' . ObjectManager::$_self->getObject('sqlAdapter')->escape($sessionId) . '"';

		$result = ObjectManager::$_self->getObject('sqlAdapter')->query($query);
		if ($result->num_rows > 0) {
			$userRecord = $result->fetch_assoc();
			if (!empty($userRecord)) {
				return $userRecord;
			}
		}
	}
}