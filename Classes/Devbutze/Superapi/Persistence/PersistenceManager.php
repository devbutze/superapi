<?php
namespace Devbutze\Superapi\Persistence;

use Devbutze\Superapi\Exception\InternalServerErrorException;
use Devbutze\Superapi\Exception\RelationNotFoundException;
use Devbutze\Superapi\Object\ObjectManager;

class PersistenceManager {

	/**
	 * @var SqlAdapter
	 */
	protected $sqlAdapter;

	public function __construct() {
		$this->sqlAdapter = ObjectManager::$_self->getObject('sqlAdapter');
	}

	/**
	 * @param string $object
	 * @param array $condition
	 * @return mixed
	 */
	public function getSubObject($object, $condition = array(), $isSubobject = TRUE) {
		return $this->getObject($object, $condition, $isSubobject);
	}

	/**
	 * @param string $object
	 * @param array $condition
	 * @return mixed
	 */
	public function getSubObjects($object, $condition = array(), $isSubobject = TRUE) {
		return $this->getObjects($object, $condition, $isSubobject);
	}

	/**
	 * @param string $object
	 * @param array $condition
	 * @return mixed
	 */
	public function getObject($object, $condition = array(), $isSubobject = FALSE) {
		return (array_pop($this->getObjects($object, $condition, $isSubobject)) ?: array());
	}

	/**
	 * @param string $object
	 * @param array $condition
	 * @param bool $isSubobject
	 * @return array
	 */
	public function getObjects($object, $condition = array(), $isSubobject = FALSE) {
		$rows = $this->getRows($object, $condition);

		$objects = array();
		foreach ($rows as $row) {
			$objects[] = ObjectManager::$_self->getObject('mapper')
				->mapRowToObject($object, $row, $isSubobject);
		}

		return $objects;
	}

	public function getRows($object, $condition = array()) {
		$query = ObjectManager::$_self->getObject('sqlQueryBuilder')->buildSelectStatement(
			$object,
			array(
			'condition' => $condition,
		));

		return ObjectManager::$_self->getObject('sqlAdapter')
			->findByQuery($query);
	}

	public function addObject($object, $data) {
		// @todo implement auth
		return $this->insertRow($object, $data);
	}

	/**
	 * @param $object
	 * @param $objectId
	 * @param $property
	 * @param $data
	 * @return int
	 * @throws RelationNotFoundException
	 */
	public function addSubObject($object, $objectId, $property, $data) {
		$fieldMap = ObjectManager::$_self->getObject('mapper')->getFieldMap($object, $property);

		// check if body data is a relation
		if ($data['id'] && count($data) === 1) {
			$result = $this->addRelation($object, $objectId, $property, $data['id']);
		} else {
			$result = $this->addObject(
				$fieldMap['relation']['to'],
				$data
			);

			if ($result) {
				$result = $this->addRelation($object, $objectId, $property, $result);
			}
		}

		return $result;
	}

	/**
	 * @param string $parentObject
	 * @param string $parentObjectId
	 * @param string $property
	 * @param string $identifier
	 * @return bool
	 * @throws InternalServerErrorException
	 * @throws RelationNotFoundException
	 */
	public function addRelation($parentObject, $parentObjectId, $property, $identifier) {
		$mapper = ObjectManager::$_self->getObject('mapper');
		$fieldMap = $mapper->getFieldMap($parentObject, $property);

		if (!$fieldMap) {
			throw new RelationNotFoundException('fieldMap not found.');
		}

		// check if given relation exists
		if (!$this->getSubObject($fieldMap['relation']['to'], array('id' => $identifier))) {
			throw new RelationNotFoundException();
		}

		switch ($fieldMap['relation']['lookup']) {
			case 'csv':
				$record = $this->getSubObject($parentObject, array('id' => $parentObjectId));
				$relations = explode(',', $record[$property]);

				if ($fieldMap['relation']['maxItems']
					&& $fieldMap['relation']['maxItems'] <= count($relations)) {
					throw new InternalServerErrorException('Maximum relation count reached.');
				}

				$relations[] = $identifier;
				$relations = trim(implode(',', array_unique($relations)), ',');
				break;
			case 'mm':
				$relationTableMap = $mapper->getTableMap($fieldMap['relation']['via']);

				$relationId = $this->insertRow(
					$fieldMap['relation']['via'],
					array(
						$relationTableMap['localKey'] => $parentObjectId,
						$relationTableMap['foreignKey'] => $identifier,
					)
				);

				$relations = $this->count(
					$fieldMap['relation']['via'],
					array($relationTableMap['localKey'] => $parentObjectId)
				);
				break;
			case 'reverse':
				$relationId = $this->updateRow(
					$fieldMap['relation']['to'],
					array('id' => $identifier),
					array(
						$fieldMap['relation']['foreignKey'] => $parentObjectId,
					)
				);

				$relations = $this->count(
					$mapper->getTablenameForObject($fieldMap['relation']['to']),
					array($fieldMap['relation']['foreignKey'] => $parentObjectId)
				);
				break;
			default: // oneToOne
				$relationId = $relations = $identifier;
		}

		$parentRelationId = $this->updateRow(
			$parentObject,
			array('id' => $parentObjectId),
			array(
				($fieldMap['nameInSource'] ?: $property) => $relations
			)
		);

		// if update went well, return the updated object's id
		return ($relationId ?: ($parentRelationId ?: $identifier));
	}

	/**
	 * @param string $object
	 * @param string $objectId
	 * @param string $property
	 * @param array $data
	 * @param string $propertyId
	 * @return bool
	 * @throws InternalServerErrorException
	 */
	public function updateSubObject($object, $objectId, $property, array $data, $propertyId = NULL) {
		$parentFieldMap = ObjectManager::$_self->getObject('mapper')->getFieldMap($object, $property);

		if (!$propertyId) {
			switch ($parentFieldMap['relation']['lookup']) {
				case 'csv':
					throw new InternalServerErrorException('Please specify a relation id.');
					break;
				default: // oneToOne
					// read the property's id from parent record
					$parentRecord = $this->getSubObject($object, array('id' => $objectId));
					$propertyId = $parentRecord[$property];
			}
		}

		$return = $this->updateRow(
			$parentFieldMap['relation']['to'],
			array('id' => $propertyId),
			$data
		);

		// if update went well, return the updated object's id
		return ($return ? $propertyId : $return);
	}

	public function removeObject($object, $objectId) {
		return $this->deleteRow($object, array('id' => $objectId));
	}

	/**
	 * At the moment we just remove the relation.
	 * @todo implement remove: cascade where the deletion of the hole child object is possible
	 *
	 * @param string $object
	 * @param string $objectId
	 * @param string $property
	 * @param string $propertyId
	 * @return bool
	 */
	public function removeSubObject($object, $objectId, $property, $propertyId) {
		$fieldMap = ObjectManager::$_self->getObject('mapper')->getFieldMap($object, $property);

		if (!empty($fieldMap['relation'])) {
			return $this->removeRelation($object, $objectId, $property, $propertyId);
		}
	}

	/**
	 * @param string $object
	 * @param string $objectId
	 * @param string $property
	 * @param string $identifier
	 * @return bool
	 * @throws InternalServerErrorException
	 */
	public function removeRelation($object, $objectId, $property, $identifier = NULL) {
		$mapper = ObjectManager::$_self->getObject('mapper');
		$fieldMap = $mapper->getFieldMap($object, $property);

		switch ($fieldMap['relation']['lookup']) {
			case 'csv':
				if (empty($identifier)) {
					throw new InternalServerErrorException('You can not flush all relations. Please specify, which relation to delete.');
				}

				$record = $this->getSubObject($object, array('id' => $objectId));
				$relations = explode(',', $record[$property]);
				if (($key = array_search($identifier, $relations)) !== FALSE) {
					unset($relations[$key]);
				}
				$relations = trim(implode(',', $relations), ',');
				break;
			case 'mm':
				$relationTableMap = $mapper->getTableMap($fieldMap['relation']['via']);

				$this->deleteRow(
					$fieldMap['relation']['via'],
					array(
						$relationTableMap['localKey'] => $objectId,
						$relationTableMap['foreignKey'] => $identifier,
					)
				);

				$relations = $this->count(
					$fieldMap['relation']['via'],
					array($relationTableMap['localKey'] => $objectId)
				);
				break;
			case 'reverse':
				$this->deleteRow(
					$fieldMap['relation']['to'],
					array(
						'id' => $identifier,
						$fieldMap['relation']['foreignKey'] => $objectId,
					)
				);

				$relations = $this->count(
					$fieldMap['relation']['to'],
					array($fieldMap['relation']['foreignKey'] => $objectId)
				);
				break;
			default: // oneToOne
				$relations = 0;
		}

		return $this->updateRow(
			$object,
			array('id' => $objectId),
			array(
				($fieldMap['nameInSource'] ?: $property) => $relations
			)
		);
	}

	public function count($object, $condition) {
		return $this->sqlAdapter->countByQuery(
			ObjectManager::$_self->getObject('sqlQueryBuilder')->buildSelectStatement(
				$object,
				array(
				'condition' => $condition,
				)
			)
		);
	}

	public function updateRow($object, $condition, $fields) {
		return $this->sqlAdapter->query(
			ObjectManager::$_self->getObject('sqlQueryBuilder')
				->buildUpdateStatement(
					$object,
					array(
					'condition' => $condition,
					'insertFields' => $fields,
				)));
	}

	public function deleteRow($object, $condition) {
		$sqlQueryBuilder = ObjectManager::$_self->getObject('sqlQueryBuilder');
		$sql = $sqlQueryBuilder->buildQueryParts(
			$object,
			array(
			'condition' => $condition,
		));

		$sql = ObjectManager::$_self->getObject('processorManager')->preProcess($object, 'delete', $sql);

		$result = $this->sqlAdapter->query($sqlQueryBuilder->buildDeleteStatement($object, $sql));

		if ($result === TRUE) {
			ObjectManager::$_self->getObject('processorManager')->postProcess($object, 'delete', array('condition' => $condition));
		}

		return $result;
	}

	public function insertRow($object, $fields) {
		$sqlQueryBuilder = ObjectManager::$_self->getObject('sqlQueryBuilder');
		$sql = $sqlQueryBuilder->buildQueryParts(
			$object,
			array(
			'insertFields' => $fields,
		));

		$sql = ObjectManager::$_self->getObject('processorManager')->preProcess($object, 'insert', $sql);

		$result = $this->sqlAdapter->query($sqlQueryBuilder->buildInsertStatement($object, $sql));

		if ($result === TRUE) {
			$result = $this->sqlAdapter->getLastInsertId();
			ObjectManager::$_self->getObject('processorManager')->postProcess($object, 'insert', array('newId' => $result, 'sql' => $sql));
		}

		return $result;
	}
}