<?php
namespace Devbutze\Superapi\Persistence;

use Devbutze\Superapi\Object\ObjectManager;
use Devbutze\Superapi\Persistence\SqlAdapter;

class SqlQueryBuilder {

	/**
	 * @var SqlAdapter
	 */
	protected $sqlAdapter;

	public function __construct() {
		$this->sqlAdapter = ObjectManager::$_self->getObject('sqlAdapter');
	}

	public function buildSelectStatement($object, array $configuration) {
		$sql = $this->buildQueryParts($object, $configuration);

		$query = 'SELECT';

		foreach ($sql['selectFields'] as $field) {
			$query .= ' ' . $field['object'] . '.' . $field['property'];
		}

		$query .= ' FROM';

		foreach ($sql['tables'] as $object => $table) {
			$query .= ' ' . $table . ' AS ' . $object;
		}

		if (!empty($sql['where'])) {
			$query .= ' WHERE (' . implode(' AND ', $sql['where']) . ')';
		}

		return $query;
	}

	public function buildDeleteStatement($object, $sql) {
		$query = 'DELETE FROM ' . array_pop($sql['tables']);

		$query .= ' WHERE (' . implode(' AND ', $sql['where']) . ')';

		return $query;
	}

	public function buildUpdateStatement($object, array $configuration) {
		$sql = $this->buildQueryParts($object, $configuration);

		$query = 'UPDATE';

		foreach ($sql['tables'] as $object => $table) {
			$query .= ' ' . $table . ' AS ' . $object;
		}

		foreach ($sql['insertFields'] as $field => $value) {
			$insert[] = ' ' . $field . ' = "' . $value . '"';
		}

		$query .= ' SET ' . implode(',', $insert);

		$query .= ' WHERE (' . implode(' AND ', $sql['where']) . ')';

		return $query;
	}

	public function buildInsertStatement($object, array $sql) {
		/**
		 * set the owner
		 * @todo please refactore this somehow
		 */
		$mappingConfiguration = ObjectManager::$_self->getObject('configurationManager')
			->getMappingConfiguration($object);
		if (NULL !== $ownerField = $mappingConfiguration['access']['owner']) {
			$sql['insertFields'][$ownerField] = ObjectManager::$_self->getObject('user')->getId();
		}

		$query = 'INSERT INTO ';

		foreach ($sql['tables'] as $table) {
			$query .= ' ' . $table;
		}

		/**
		 * deal with fixedValues configured
		 * because this is only done on INSERT,
		 * this is not done by buildInsertFields
		 */
		if (!empty($mappingConfiguration['fixedValues'])) {
			$fixedValues = $this->parseFixedValues($mappingConfiguration['fixedValues']);
			$sql['insertFields'] = array_merge(
				$sql['insertFields'],
				$fixedValues
			);
		}

		foreach ($sql['insertFields'] as $field => $value) {
			$insert[] = ' ' . $field . ' = "' . $value . '"';
		}

		$query .= ' SET ' . implode(',', $insert);

		return $query;
	}

	public function buildQueryParts($object, $configuration) {
		$configuration = array_merge(
			array(
				'selectFields' => array(),
				'object' => array(),
				'insertFields' => array(),
				'condition' => array(),
				'limit' => 0,
			),
			$configuration
		);

		$sql['selectFields'] = ($configuration['selectFields'] ?: array(array('object' => $object, 'property' => '*')));

		$sql['insertFields'] = $this->buildInsertFields($object, $configuration['insertFields']);

		// convert FROM-statement from object notation to table names
		$sql['tables'] = $this->buildTables($object);

		// convert WHERE-statement from object notation to table names
		$sql['where'] = $this->buildWhere($object, $configuration['condition']);

		return $sql;
	}

	protected function buildTables($object) {
		if (!is_array($object)) {
			$return[$object] = $object;

			$tableMap = ObjectManager::$_self->getObject('mapper')
				->getTableMap($object);
			if (isset($tableMap['nameInSource'])) {
				$return[$object] = $tableMap['nameInSource'];
			}
		} else {
			die('@todo multiple tables not yet supported');
		}

		return $return;
	}

	protected function buildInsertFields($object, $fields) {
		$mappingConfiguration = ObjectManager::$_self->getObject('configurationManager')
			->getMappingConfiguration($object);
		$return = array();

		foreach ($fields as $fieldName => $fieldValue) {
			if (!empty($mappingConfiguration['hiddenFields'])
				&& in_array($fieldName, $mappingConfiguration['hiddenFields'])) {
				continue;
			}

			$return[$fieldName] = $this->sqlAdapter->escape($fieldValue);
		}

		return $return;
	}

	protected function buildWhere($object, $constraints, $nested = FALSE) {
		$return = '';

		if (!$nested) {
			$mappingConfiguration = ObjectManager::$_self->getObject('configurationManager')
				->getMappingConfiguration($object);

			if (!empty($mappingConfiguration['constraints'])) {
				$constraints = array_merge(
					$constraints,
					$mappingConfiguration['constraints']
				);
			}
		}

		foreach ($constraints as $field => $constraint) {
			if ($field === 'and') {
				$subConstraints = $this->buildWhere($object, $constraint, TRUE);
				$return[] = '(' . implode(' AND ', $subConstraints) . ')';
			} elseif ($field === 'or') {
				$subConstraints = $this->buildWhere($object, $constraint, TRUE);
				$return[] = '(' . implode(' OR ', $subConstraints) . ')';
			} else {
				if (!strstr($field, '.')) {
					$field = ObjectManager::$_self->getObject('mapper')
						->getFieldnameForProperty($object, $field);

					// @todo erm?! escape please
					if (is_array($constraint)) {
						$return[] = $field . ' IN (' . implode(', ', $constraint) . ')';
					} else {
						$return[] = $field . ' = \'' . $constraint . '\'';
					}
				} else {
					die('conditions on sub-properties not yet supported.');
				}
			}
		}

		return $return;
	}

	protected function parseFixedValues($fixedValues) {
		$return = $fixedValues;

		foreach ($fixedValues as $field => $value) {
			switch ($value) {
				case '_TIMESTAMP_':
					$return[$field] = time();
					break;
			}
		}

		return $return;
	}
}