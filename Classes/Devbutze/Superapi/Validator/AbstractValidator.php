<?php
namespace Devbutze\Superapi\Validator;

/**
 * Class AbstractObjectProcessor
 *
 * may implement pre/post insert/select/update/delete
 *
 * @package Devbutze\Superapi\Processor
 */
abstract class AbstractValidator {
}