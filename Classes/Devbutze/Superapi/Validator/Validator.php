<?php
namespace Devbutze\Superapi\Validator;
use Devbutze\Superapi\Object\ObjectManager;

/**
 * Class AbstractObjectProcessor
 *
 * may implement pre/post insert/select/update/delete
 *
 * @package Devbutze\Superapi\Processor
 */
class Validator {

	public function validate($objectName, $method, $objectData) {
		$validatorName = ObjectManager::$_self->getObject('configurationManager')->getNamespace()
			. '\Validator\\' . ucfirst($objectName) . 'Validator';
		if (class_exists($validatorName)) {
			$validator = new $validatorName();
			$validatorMethod = 'validate' . ucfirst($method);
			if (method_exists($validator, $validatorMethod)) {
				$result = call_user_func(array($validator, $validatorMethod), $objectData);

					if (!$result['status']) {
						$result['status'] = 'failed';
						$response = ObjectManager::$_self->getObject('response');
						$response->setData($result);
						$response->send();
					exit;
				}
			}
		}
	}
}